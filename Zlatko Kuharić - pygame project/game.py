import pygame
import random

# Inicijaliziranje pygame-a
pygame.init()

# Definiranje boja
GREEN = (57, 255, 20)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
LIGHTBLUE = (135, 206, 235)
DARKBLUE = (0, 49, 82)

font = pygame.font.Font(None, 36) #učitavanje fonta
 
# Klase

class Ufo(pygame.sprite.Sprite):
    """ Ova klasa predstavlja ufo. """
    def __init__(self, color):
        super().__init__()
        self.image = pygame.image.load('ufo.png') #Ucitavanje ufo
        self.rect = self.image.get_rect()
 
 
class Player(pygame.sprite.Sprite):
    """ Ova klasa predstavlja: Player-a odnosno spaceship. """
    def __init__(self):
        """ Priprema igrača za kreiranje. """
        super().__init__()
        self.image = pygame.image.load('spaceship.png') #Ucitavanje spaceshipa
        self.rect = self.image.get_rect()
 
    def update(self):
        """ Update player-ove pozicije. """
        # Dohvati poziciju pokazivača. Dohvaća poziciju kao listu dva broja.
        pos = pygame.mouse.get_pos()
 
        # Postavlja poziciju x igrača u poziciju x pokazivača
        self.rect.x = pos[0]
 
 
class Metak(pygame.sprite.Sprite):
    """ Ova klasa predstavlja metak. """
    def __init__(self):
        super().__init__()
        self.image = pygame.Surface([4, 10])
        self.image.fill(GREEN)
        self.rect = self.image.get_rect()
 
    def update(self):
        """ Pomicanje metka. """
        self.rect.y -= 3
 
 
# KREIRANJE PROZORA IGRE
 

# Postavljanje visine i  širine prozora
screen_width = 700
screen_height = 400
screen = pygame.display.set_mode([screen_width, screen_height])
pygame.display.set_caption("Space Shooter")
 
# --- Sprites lista
 
# Lista svakog blokića u igri. 
all_sprites_list = pygame.sprite.Group()
 
# Lista pojedinog ufo-a
ufo_list = pygame.sprite.Group()
 
# Lista pojedinog metka
metak_list = pygame.sprite.Group()
 
# KREIRANJE Spritesa
 
for i in range(40):
    # Ovo predstavlja ufo
    ufo = Ufo(BLUE)
 
    # Postavljanje random lokacije blokića
    ufo.rect.x = random.randrange(screen_width)
    ufo.rect.y = random.randrange(350)
 
    # Dodaje blok listi objekata
    ufo_list.add(ufo)
    all_sprites_list.add(ufo)
 
# Kreiranje playera/spaceshipa pomoću kojeg igram
player = Player()
all_sprites_list.add(player)
 
# Loop sve dok se ne klikne x button
done = False
 
clock = pygame.time.Clock()
 
score = 0
state = "menu" # ili "play" 
player.rect.y = 370
 
 # dodavanje pozadinske slike i zvuka
background_image = pygame.image.load("zvjezdana_noc.jpg").convert()
click_sound = pygame.mixer.Sound("laserv2.wav")


# -------- LOOP GLAVNOG PROGRAMA -----------
while not done:
    # --- Procesiranje događaja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
 
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if state == "menu":
                if play_game.collidepoint(event.pos):
                    state = "play"
            if state == "play":
                # Klik miša = ispaljen metak
                metak = Metak()
                # Metak se ispaljuje od tamo gdje se nalazi igrač
                metak.rect.x = player.rect.x
                metak.rect.y = player.rect.y
                # Dodavanje metka u listu
                all_sprites_list.add(metak)
                metak_list.add(metak)
        # Dodavanje zvuka prilikom klika miša
                click_sound.play()

            


    if state == "play":
        # POZADINSKA SLIKA
        screen.blit(background_image, [0,0])
        score_text = font.render("Score: %d"%score, True, WHITE)
        screen.blit(score_text, [10,20])
        
        # Kalkuliranje mehanike svakog metka
        for metak in metak_list:
 
            # Pogledati ako je metak pogodio blok
            ufo_hit_list = pygame.sprite.spritecollide(metak, ufo_list, True)
 
            # ZA svaki pogođeni blok, maknuti metak i dodati u score
            for ufo in ufo_hit_list:
                metak_list.remove(metak)
                all_sprites_list.remove(metak)
                score += 1
                print(score)
 
            # Metak nestaje ako prođe (ništa ne pogodi na screenu)
            if metak.rect.y < -10:
                metak_list.remove(metak)
                all_sprites_list.remove(metak)

        # iscrtavanje svih spritesa
        all_sprites_list.draw(screen)
        # Update na svim spritesima
        all_sprites_list.update()
        if score == 40:
            state = "gameover"

    elif state == "menu":
        screen.fill(LIGHTBLUE)
        play_game = pygame.image.load('start.jpg') #Ucitavanje start
        play_game = screen.blit(play_game, (200,100))

    elif state == "gameover":
        screen.fill(DARKBLUE)
        score_text = font.render("Uspješno ste završili igricu!!", True, LIGHTBLUE)
        screen.blit(score_text, [200,100])
        over_game = pygame.image.load('gameover.png') #Ucitavanje gameover slike
        over_game = screen.blit(over_game, (250,150))
        

 
    # Update screena sa svime što smo nacrtali
    pygame.display.flip()
 
    # Limit za 20 fpsa 
    clock.tick(60)
 
pygame.quit()
