ls =>  prikaži sadržaje direktorija

cd DIREKTORIJ => uđi u direktorij

git clone REPOZITORIJ => kloniraj repozitorij

git status => prikazuje status, postoji li izmjena?

git add IME_FILEA => dodati ime filea

git log => prikazati sve commitove

git commit -m "PORUKA" => ispisati poruku

git push origin master => nakon toga upisujemo username i password