## curriculum vitae

 

## OSOBNI PODACI

 
IME I PREZIME: MARI0 MARIĆ

ADRESA: Laginjina 1, 51000 Rijeka

MOBITEL: 091 555 4343

E-MAIL: mmaric@gmail.com

DATUM ROĐENJA: 11. siječanj 1982.



## OBRAZOVANJE

VRIJEME (OD – DO)

1996. – 2000.

NAZIV I OBLIK ORGANIZACIJE: Ekonomska škola Rijeka

NAZIV OSTVARENE KVALIFIKACIJE / POSTIGNUĆA: Ekonomski tehničar

RAZINA U NACIONALNOJ KLASIFIKACIJI: SSS

## DODATNO OBRAZOVANJE

 

TEČAJEVI: Informatički tečajevi, Tečaj poslovnog engleskog jezika



## RADNO ISKUSTVO

VRIJEME (OD – DO)

    Siječanj 2001. – Prosinac 2009.

NAZIV I DJELATNOST POSLODAVCA

    Alfa d.o.o.

Poduzeće za financije, konsulting i poslovnu edukaciju


RADNO MJESTO
Tajnik

GLAVNI ZADACI I ODGOVORNOSTI

 Razvoj i vođenje svakodnevnog poslovanja

Organizacija urednog uredskog poslovanja

Marketing aktivnosti

OSOBNE VJEŠTINE I KOMPETENCIJE

 

## STRANI JEZICI: 

Engleski (aktivno)

 

 Talijanski (pasivno)

## TEHNIČKE VJEŠTINE I KOMPETENCIJE

 
Windows operativni sustavi

-Microsoft Office (Word, Excel, Access, PowerPoint, Outlook)

Korištenje Interneta i e-mail-a

 

 

 

## VOZAČKA DOZVOLA: B