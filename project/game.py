import pygame
import random
 
# Definiranje boja
GREEN = (57, 255, 20)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
 
# Klase

class Block(pygame.sprite.Sprite):
    """ Ova klasa predstavlja blok. """
    def __init__(self, color):
        
        super().__init__()
 
        self.image = pygame.Surface([20, 15])
        self.image.fill(color)
 
        self.rect = self.image.get_rect()
 
 
class Player(pygame.sprite.Sprite):
    """ Ova klasa predstavlja: Player-a. """
 
    def __init__(self):
        """ Priprema igrača za kreiranje. """
        
        super().__init__()
 
        self.image = pygame.Surface([20, 20])
        self.image.fill(RED)
 
        self.rect = self.image.get_rect()
 
    def update(self):
        """ Update player-ove pozicije. """
        # Dohvati poziciju pokazivača. Dohvaća poziciju kao listu dva broja.
        pos = pygame.mouse.get_pos()
 
        # Postavlja poziciju x igrača u poziciju x pokazivača
        self.rect.x = pos[0]
 
 
class Bullet(pygame.sprite.Sprite):
    """ Ova klasa predstavlja metak. """
    def __init__(self):
        super().__init__()
 
        self.image = pygame.Surface([4, 10])
        self.image.fill(GREEN)
 
        self.rect = self.image.get_rect()
 
    def update(self):
        """ Pomicanje metka. """
        self.rect.y -= 3
 
 
# KREIRANJE PROZORA IGRE
 
# Inicijaliziranje pygame-a
pygame.init()

# Postavljanje visine i  širine prozora
screen_width = 700
screen_height = 400
screen = pygame.display.set_mode([screen_width, screen_height])
pygame.display.set_caption("Pucačina ︻┻┳══━一一一")
 
# --- Sprites lista
 
# Lista svakog blokića u igri. 
all_sprites_list = pygame.sprite.Group()
 
# Lista pojedinog bloka 
block_list = pygame.sprite.Group()
 
# Lista pojedinog metka
bullet_list = pygame.sprite.Group()
 
# KREIRANJE Spritesa
 
for i in range(50):
    # Ovo predstavlja blok
    block = Block(BLUE)
 
    # Postavljanje random lokacije blokića
    block.rect.x = random.randrange(screen_width)
    block.rect.y = random.randrange(350)
 
    # Dodaje blok listi objekata
    block_list.add(block)
    all_sprites_list.add(block)
 
# Kreiranje bloka pomoću kojeg igram
player = Player()
all_sprites_list.add(player)
 
# Loop sve dok se ne klikne x button
done = False
 
clock = pygame.time.Clock()
 
score = 0
player.rect.y = 370
 
 # dodavanje pozadinske slike i zvuka
background_image = pygame.image.load("zvjezdana_noc.jpg").convert()
click_sound = pygame.mixer.Sound("laserv2.wav")


# -------- LOOP GLAVNOG PROGRAMA -----------
while not done:
    # --- Procesiranje događaja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
 
        elif event.type == pygame.MOUSEBUTTONDOWN:
            # Klik miša = ispaljen metak
            bullet = Bullet()
            # Metak se ispaljuje od tamo gdje se nalazi igrač
            bullet.rect.x = player.rect.x
            bullet.rect.y = player.rect.y
            # Dodavanje metka u listu
            all_sprites_list.add(bullet)
            bullet_list.add(bullet)
            # Dodavanje zvuka prilikom klika miša
            click_sound.play()


 
    # --- LOGIKA IGRE
 
    # Update na svim spritesima
    all_sprites_list.update()
 
    # Kalkuliranje mehanike svakog metka
    for bullet in bullet_list:
 
        # Pogledati ako je metak pogodio blok
        block_hit_list = pygame.sprite.spritecollide(bullet, block_list, True)
 
        # ZA svaki pogođeni blok, maknuti metak i dodati u score
        for block in block_hit_list:
            bullet_list.remove(bullet)
            all_sprites_list.remove(bullet)
            score += 1
            print(score)
 
        # Metak nestaje ako prođe (ništa ne pogodi na screenu)
        if bullet.rect.y < -10:
            bullet_list.remove(bullet)
            all_sprites_list.remove(bullet)
 
    # --- Crtanje frejmova
 
    # POZADINSKA SLIKA
    screen.blit(background_image, [0,0])

    # iscrtavanje svih spritesa
    all_sprites_list.draw(screen)
 
    # Update screena sa svime što smo nacrtali
    pygame.display.flip()
 
    # Limit za 20 fpsa 
    clock.tick(60)
 
pygame.quit()