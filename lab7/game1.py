# ukljucivanje biblioteke pygame
import pygame

pygame.init()
# definiranje konstanti za velicinu prozora
WIDTH = 800
HEIGHT = 600

RED = [ 255, 0, 0 ]
GREEN = [ 0, 255, 0 ]
BLUE = [ 0, 0, 255 ]
TIRKIZ = [ 64,224,208 ]
MAGENTA = [ 255, 0, 255 ]
YELLOW = [ 255, 255, 0 ]
BLACK = [ 0, 0, 0]

slika_bg = pygame.image.load("slika.jpeg")
slika_bg = pygame.transform.scale(slika_bg, (WIDTH, HEIGHT))
slika1_bg = pygame.image.load("slika1.jpeg")
slika1_bg = pygame.transform.scale(slika1_bg, (WIDTH, HEIGHT))
slika2_bg = pygame.image.load("slika2.jpeg")
slika2_bg = pygame.transform.scale(slika2_bg, (WIDTH, HEIGHT))
slika3_bg = pygame.image.load("slika3.jpeg")
slika3_bg = pygame.transform.scale(slika3_bg, (WIDTH, HEIGHT))
slika4_bg = pygame.image.load("slika4.jpeg")
slika4_bg = pygame.transform.scale(slika4_bg, (WIDTH, HEIGHT))

myfont = pygame.font.SysFont('Courier', 50)
slika_text = myfont.render("Doberman 1", False, YELLOW)
slika1_text = myfont.render("Doberman 2", False, BLACK)
slika2_text = myfont.render("Doberman 3", False, BLACK)
slika3_text = myfont.render("Doberman 4", False, YELLOW)
slika4_text = myfont.render("Doberman 5", False, BLACK)


# tuple velicine prozora
size = (WIDTH, HEIGHT)

#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("Dobermans")

clock = pygame.time.Clock()
i = 0
duration = 0
done = False
while not done:
    #event petlja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                bg_color = next_bg(bg_color)
    #i += 1
    if duration < 0 and i == 0:
        duration = 10000
        screen.blit(slika_bg, (0,0))
        screen.blit(slika_text, (0,0))
        i = 1
    elif duration < 0 and i == 1:
        duration = 10000
        screen.blit(slika1_bg, (0,0))
        screen.blit(slika1_text, (0,0))
        i = 2
    elif duration < 0 and i == 2:
        duration = 10000
        screen.blit(slika2_bg, (0,0))
        screen.blit(slika2_text, (0,0))
        i = 3
    elif duration < 0 and i == 3:
        duration = 10000
        screen.blit(slika3_bg, (0,0))
        screen.blit(slika3_text, (0,0))
        i = 4
    elif duration < 0 and i == 4:
        duration = 10000
        screen.blit(slika4_bg, (0,0))
        screen.blit(slika4_text, (0,0))
        i = 0

    pygame.display.flip()
    
    #ukoliko je potrebno ceka do iscrtavanja 
    #iduceg framea kako bi imao 60fpsa
    time = clock.tick(60)
    duration = duration-time
pygame.quit()